<!SLIDE >
# who / our group
## docker community saint louis
A Docker sponsored community, this one of over a hundred in cities all over the world!

Goals:   

* Host speakers and demos like this one
* Help organize Docker Hackathons and tech workshops
* Give members info and access to neat, upcoming Docker related technology
* Provide support to members interested in contributing to Docker related projects


<!SLIDE >
# who / our host
## wwt asynchrony labs
Asynchrony Labs is the software development arm of it's parent company, World
World Technologies. Asynchrony Labs develops custom software solutions
to lots of neat companies (that they often can't talk about). Asynchrony also
provides Agile coaching and delivery team mentoring. You'll see *many*
Asynchronites hosting interesting Meetups and events across the city.

World Wide Technology is one of the world's largest providers of Cloud and
Telecom infrastructure hardware and integrated rack solutions. WWT has called
St. Louis home for over 20 years.

~~~SECTION:notes~~~
Thank Asynchrony for hosting and providing drinks and pizza! WWT will be hosting
next Meetup.
~~~ENDSECTION~~~

<!SLIDE >
# who / the devs
## Team Wookie Monster
<img src="../_images/wookiee_monster_final.png" width="50%" height="50%">

Asynchrony's Onboarding Team, where developers new to the company learn how
to do things the "Asynchrony Way", all while also striving to do things the "best" way.

~~~SECTION:notes~~~
Asynchrony Way
- Pair Programming
- Kanban
- Experiment
~~~ENDSECTION~~~
